<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('monitoramento/status', function () {
//     // // Verifica Brasil Cidadão
//     // $ch = curl_init('https://sso.acesso.gov.br/');
//     // curl_setopt($ch, CURLOPT_TIMEOUT, 5);
//     // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
//     // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//     // curl_exec($ch);
//     // $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//     // curl_close($ch);
//     // Verifica Banco de Dados
//     // $db = Contrato::first();

//     // $status_geral = ( $db <> null && $httpcode >= 200 && $httpcode < 400 ) ? 'ok' : 'ERRO';
//     // $status_geral = $db <> null ? 'ok' : 'ERRO';
//     $status_geral = 'ok';

//     $json = [
//         'ambiente' => App::environment(),
//         'url' => $_SERVER['HTTP_HOST'],
//         'banco_de_dados' => $db <> null ? 'ok' : 'ERRO',
//         // 'brasil_cidadao' => ($httpcode >= 200 && $httpcode < 400) ? 'ok' : 'ERRO',
//         'Status Geral' => $status_geral
//     ];

//     $response = \Response::json($json, $status_geral <> 'ok' ? 503 : 200);
//     $response->header('status_geral', $status_geral);

//     return $response;
// });
